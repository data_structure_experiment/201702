#ifndef INC_201702_DOUBLE_LINKED_LIST_HPP
#define INC_201702_DOUBLE_LINKED_LIST_HPP

#include <algorithm>
#include <cassert>
#include <memory>

template <typename T>
class double_linked_list //线性表的双向链表实现.
{

    class node
    {
    public:
        using value_type = T;

        node(value_type const &v, node *prev, std::unique_ptr<node> next)
            : value(v), prev(prev), next(std::move(next))
        {}

        value_type value;
        node *prev;
        std::unique_ptr<node> next;
    };

public:
    using node_type = node; //存储元素的数组类型
    using value_type = T; //元素类型
    using handler_type = std::unique_ptr<node_type>; //管理内存的智能指针.
    using size_type = std::size_t; //可以用来储存索引和列表长度的类型.

    struct base_iter //迭代器基类, 包含满足双向迭代器的类型定义.
    {
        using difference_type = std::ptrdiff_t;
        using value_type = T;
        using pointer = T *;
        using reference = T &;
        using iterator_category = std::bidirectional_iterator_tag;
    };

    class iterator;

    class const_iterator : public base_iter //只读迭代器
    {
        friend class iterator;

        friend class double_linked_list;

        const_iterator(const double_linked_list *l, node_type *p)
            : plist(l), node(p)
        {}

    public:
        value_type const &operator*() const
        {
            return node->value;
        }

        value_type const *operator->() const
        {
            return &node->value;
        }

        const_iterator &operator++()
        {
            node = node->next.get();
            return *this;
        }

        const_iterator operator++(int)
        {
            const_iterator ret = *this;
            ++*this;
            return ret;
        }

        const_iterator &operator--()
        {
            if (node == nullptr)
                node = plist->tail;
            else
                node = node->prev;
            return *this;
        }

        const_iterator operator--(int)
        {
            const_iterator ret = *this;
            --*this;
            return ret;
        }

        bool operator==(const_iterator const &iter) const
        {
            return node == iter.node;
        }

        bool operator!=(const_iterator const &iter) const
        {
            return !(*this == iter);
        }

    private:
        const double_linked_list *plist;
        node_type *node;
    };

    class iterator : public base_iter //读写迭代器
    {
        friend class double_linked_list;

        iterator(double_linked_list *l, node_type *p)
            : plist(l), node(p)
        {}

    public:
        operator const_iterator() const
        {
            return const_iterator(plist, node);
        }

        value_type &operator*() const
        {
            return node->value;
        }

        value_type *operator->() const
        {
            return &node->value;
        }

        iterator &operator++()
        {
            node = node->next.get();
            return *this;
        }

        iterator operator++(int)
        {
            iterator ret = *this;
            ++*this;
            return ret;
        }

        iterator &operator--()
        {
            if (node == nullptr)
                node = plist->tail;
            else
                node = node->prev;
            return *this;
        }

        iterator operator--(int)
        {
            iterator ret = *this;
            --*this;
            return ret;
        }

        bool operator==(iterator iter) const
        {
            return node == iter.node;
        }

        bool operator!=(iterator iter) const
        {
            return !(*this == iter);
        }

    private:
        double_linked_list *plist;
        node_type *node;
    };

    double_linked_list() = default; //默认构造函数
    explicit double_linked_list(size_type size) //将列表初始化为长度为 size 的表
        : size_(0)
    {
        for (int i = 0; i < size; ++i)
        {
            insert(this->end(), value_type{});
        }
    }

    template <typename U>
    explicit double_linked_list(std::initializer_list<U> list) //以初始化列表 list 中的元素初始化该表
    {
        for (auto const &each : list)
            insert(end(), each);
    }

    ~double_linked_list() = default;

    double_linked_list(double_linked_list &&src) = default; //移动构造函数
    double_linked_list(double_linked_list const &src) //拷贝构造函数
    {
        for (auto const &each : src)
            insert(end(), each);
    }

    double_linked_list &operator=(double_linked_list &&src) = default; //移动赋值运算符
    double_linked_list &operator=(double_linked_list const &src) //拷贝赋值运算符
    {
        *this = double_linked_list(src); //通过拷贝构造临时对象并将其移动给自己,以达到赋值目的.
        return *this;
    }

    template <typename U>
    double_linked_list &operator=(std::initializer_list<U> list) //将初始化列表 list 中的元素赋给链表
    {
        *this = double_linked_list(list);
        return *this;
    }

    void reset(size_type size)  //将列表重新设为长度为 size 的列表.
    {
        *this = double_linked_list(size);
    }

    void clear() //清空列表, 列表变为空表.
    {
        reset(0);
    }

    bool is_empty() const noexcept //判断列表是否为空
    {
        return size_ == 0;
    }

    size_type size() const noexcept //获取列表长度.
    {
        return size_;
    }

    value_type &operator[](size_type index) //获取列表中索引为 index 的元素, 索引从0开始. 时间复杂度为O(index)
    {
        using namespace std::literals;
        if (index > size())
            throw std::out_of_range("In operator[] of double_linked_list with index "s + std::to_string(index));
        auto iter = std::next(begin(), index);
        return *iter;
    }

    value_type const &operator[](size_type index) const //同上,只读重载版本
    {
        return const_cast<double_linked_list &>(*this)[index];
    }

    template <typename pred>
    auto find(pred p) const noexcept(noexcept(p(std::declval<value_type>()))) //在列表中,对于每一个元素e, 返回第一个满足bool(p(e))为true的元素的迭代器.
    {
        //        return std::find_if(begin(), end(), p);
        for (auto iter = begin(); iter != end(); ++iter)
        {
            if (p(*iter))
                return iter;
        }
        return end();
    }

    template <typename U>
    iterator insert(const_iterator iter, U &&u) //在列表中索引为pos的位置插入u.
    {
        if (iter != end())
            return insert_before_end(iter, std::forward<U>(u));
        else
            return insert_at_end(u);
    }

    iterator erase(const_iterator iter) noexcept //移除列表中索引为pos的元素.
    {
        if (iter.node->next)
            return remove_before_tail(iter);
        else
            return remove_tail();
    }

    template <typename Visit>
    void iterate(Visit visit) //对列表中的每一个元素e, 调用visit(e).
    {
        for (auto iter = begin(); iter != end(); ++iter)
            visit(*iter);
    }

    iterator begin() noexcept //以下6个函数为迭代器.
    {
        return get_iter(head.get());
    }

    const_iterator begin() const noexcept
    {
        return get_const_iter(head.get());
    }

    const_iterator cbegin() const
    {
        return begin();
    }

    iterator end() noexcept
    {
        return get_iter(nullptr);
    }

    const_iterator end() const noexcept
    {
        return get_const_iter(nullptr);
    }

    const_iterator cend() const
    {
        return end();
    }

    template <typename U>
    friend bool operator==(double_linked_list<T> const &lhs,
                           double_linked_list<U> const &rhs) // 判断列表lhs与rhs相等.对于两个列表中的元素a和b, 使用std::equal_to<void>(a, b)判断元素相等性.
    {
        if (lhs.size() != rhs.size())
            return false;
        for (auto i = 0; i < lhs.size(); ++i)
        {
            if (!std::equal_to<void>()(lhs[i], rhs[i]))
                return false;
        }
        return true;
    }

private:
    iterator get_iter(node *n)   //由结点指针构造指向该结点的迭代器
    {
        return iterator(this, n);
    }

    const_iterator get_const_iter(node *n) const //同上, const重载版本
    {
        return const_iterator(this, n);
    }

    template <typename U>
    iterator insert_before_end(const_iterator iter, U &&value) //在iter指向元素之前插入value, 其中iter != end()
    {
        if (iter.node->prev) // insert at middle
            return insert_at_middle(iter, std::forward<U>(value));
        else // insert at begin
        {
            return insert_at_begin(value);
        }
    }

    template <typename U>
    iterator insert_at_middle(const_iterator iter, U &&value) //同上,但是iter != end() && iter != begin()
    {
        auto prev = iter.node->prev;
        prev->next = make_node(std::forward<U>(value), prev, std::move(prev->next));
        ++size_;
        iter.node->prev = prev->next.get();
        return get_iter(prev->next.get());
    }

    template <typename U>
    iterator insert_at_begin(U &&value) //在链表开头插入value
    {
        head = make_node(std::forward<U>(value), nullptr, std::move(head));
        ++size_;
        head->next->prev = head.get();
        return begin();
    }

    template <typename U>
    iterator insert_at_end(U &&value) //在链表末尾插入value
    {
        if (!is_empty()) // not empty
        {
            tail->next = make_node(std::forward<U>(value), tail, nullptr);
            ++size_;
            tail = tail->next.get();
        } else //is empty
        {
            head = make_node(std::forward<U>(value), nullptr, nullptr);
            assert(size_ == 0);
            size_ = 1;
            tail = head.get();
        }
        return get_iter(tail);
    }

    iterator remove_before_tail(const_iterator iter) //删除iter指向的元素, 保证iter != std::prev(end())
    {
        if (iter.node->prev)
            return remove_at_middle(iter);
        else
            return remove_at_begin();
    }

    iterator remove_at_middle(const_iterator iter) //删除iter指向的元素, 保证iter != begin() && iter != std::prev(end())
    {
        auto current = iter.node;
        auto prev = current->prev;
        auto next = std::move(current->next);
        next->prev = prev;
        prev->next = std::move(next);
        --size_;
        return get_iter(prev->next.get());
    }

    iterator remove_at_begin()  //删除链表首元素
    {
        head = std::move(head->next);
        head->prev = nullptr;
        --size_;
        return begin();
    }

    iterator remove_tail() //删除链表末尾元素
    {
        auto prev = tail->prev;
        if (prev)
            prev->next.reset();
        else
        {
            assert(size_ == 1);
            head.reset();
        }
        tail = prev;
        --size_;
        return end();
    }

    template <typename... Args>
    auto make_node(Args &&... args)
    {
        return std::make_unique<node>(std::forward<Args>(args)...);
    }

    handler_type head;
    node *tail = nullptr;
    size_type size_ = 0;
};

#endif //INC_201702_DOUBLE_LINKED_LIST_HPP
