#include "test_adpater.hpp"
#include "../list_adapter.hpp"

void test_adapter()
{
    list_adapter<std::string> list;
    std::string result;
    assert(list.InitalList() == OK);
    assert(list.ListEmpty() == OK);
    list.ListLength();
    list.GetElem(1, result);
    list.LocateElem(result, std::equal_to<void>());
    list.PriorElem(result, result);
    list.NextElem(result, result);
    list.ListInsert(1, result);
    list.ListDelete(1, result);
    list.ListTraverse([](auto s)
    {});
    list.ClearList();
    list.DestroyList();
}
