#include <vector>
#include "test_list.hpp"
#include "../double_linked_list.hpp"

namespace
{
    template <typename T>
    void test_size(double_linked_list<T> const &list, size_t size)
    {
        assert(list.size() == size);
        assert(std::distance(list.begin(), list.end()) == size);
    }
    template <typename U, typename T>
    void test_equal(double_linked_list<T> const &list, std::initializer_list<U> i)
    {
        bool result = (list == double_linked_list<std::string>(i));
        assert(result);
    }
}
void test_list()
{
    using namespace std::literals;
    {
        double_linked_list<std::string> list(5);
        test_size(list, 5);
        list = {"0", "1", "3", "4"};
        test_size(list, 4);
        list.insert(std::next(list.begin(), 2), "2");
        test_size(list, 5);
        test_equal(list, {"0", "1", "2", "3", "4"});
        list.insert(std::next(list.begin(), 5), "5");
        test_size(list, 6);
        test_equal(list, {"0", "1", "2", "3", "4", "5"});
        list.erase(std::next(list.begin(), 1));
        test_size(list, 5);
        test_equal(list, {"0", "2", "3", "4", "5"});
        {
            auto iter = list.find([](auto const & s)
            {
                return s == "2";
            });
            assert(iter == std::next(list.begin()));
        }
        {
            using vector_t = std::vector<std::string>;
            vector_t vec;
            list.iterate([&](auto const & s)
            {
                vec.push_back(s);
            });
            bool equal = (vec == vector_t{"0", "2", "3", "4", "5"});
            assert(equal);
        }
        list.clear();
        test_size(list, 0);
        assert(list.is_empty());
        test_equal<char *>(list, {});
    }

}
