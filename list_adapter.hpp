#ifndef INC_201702_LIST_ADAPTER_HPP
#define INC_201702_LIST_ADAPTER_HPP

#include <optional>
#include <istream>
#include <ostream>
#include <functional>
#include "double_linked_list.hpp"

#define TRUE 1
#define FALSE 0
#define OK 1
#define ERROR -2

template <typename T>
class list_adapter
{
    std::optional<double_linked_list<T>> list;  //给列表增加"列表不存在"的状态.
public:
    auto InitalList()
    {
        if (list)
            return ERROR;
        list = double_linked_list<T>();
        return OK;
    }

    auto DestroyList()
    {
        if (!list)
            return ERROR;
        list.reset();
        return OK;
    }

    auto ClearList()
    {
        if (!list)
            return ERROR;
        list->clear();
        return OK;
    }

    int ListEmpty() const noexcept
    {
        if (!list)
            return ERROR;
        else
            return list->is_empty();
    }

    int ListLength() const noexcept
    {
        if (!list)
            return ERROR;
        return list->size();
    }

    auto GetElem(std::size_t index, T &result) const
    {
        if (!list)
            return ERROR;
        if (index < 1 || index > list->size())
            return ERROR;
        result = (*list)[index - 1];
        return OK;
    }

    template <typename Equal>
    int LocateElem(T const &e, Equal compare) const
    {
        using namespace std::placeholders;
        if (!list)
            return 0;
        auto iter = list->find(std::bind(compare, e, _1));
        if (iter == list->end())
            return 0;
        return (int)std::distance(list->begin(), iter) + 1;
    }

    auto PriorElem(T const &cur, T &prev) const
    {
        if (!list)
            return ERROR;
        auto iter = list->find([&cur](auto e)
                              { return cur == e; });
        if (iter == list->begin())
            return ERROR;
        else if (iter == list->end())
            return ERROR;
        else
        {
            prev = *std::prev(iter);
            return OK;
        }
    }

    auto NextElem(T const &cur, T &next) const
    {
        if (!list)
            return ERROR;
        auto iter = list->find([&cur](auto e)
                              { return cur == e; });
        if (iter == std::prev(list->end()))
            return ERROR;
        else if (iter == list->end())
            return ERROR;
        else
        {
            next = *std::next(iter);
            return OK;
        }
    }

    auto ListInsert(std::size_t index, T const &e)
    {
        if (!list)
            return ERROR;
        if (index < 1 || index > list->size() + 1)
            return ERROR;
        list->insert(std::next(list->begin(), index - 1), e);
        return OK;
    }

    auto ListDelete(std::size_t index, T &result)
    {
        if (!list || ListEmpty())
            return ERROR;
        if (index < 1 || index > list->size())
            return ERROR;
        auto iter = std::next(list->begin(), index - 1);
        result = std::move(*iter);
        list->erase(iter);
        return OK;
    }

    template <typename Callable>
    auto ListTraverse(Callable callable)
    {
        if (!list)
            return ERROR;
        list->iterate(callable);
        return OK;
    }

    friend std::ostream &operator<<(std::ostream &out, list_adapter<T> const &list)
    {
        if (list.list)
            out << 1 << " " << *list.list;
        else
            out << 0 << " ";
        return out;
    }

    friend std::istream &operator>>(std::istream &in, list_adapter<T> &list)
    {
        list.list = double_linked_list<T>();
        int exists;
        in >> exists;
        if (exists)
        {
            in >> *list.list;
        }
        else
            list.list.reset();
        return in;
    }
};


#endif //INC_201702_LIST_ADAPTER_HPP
