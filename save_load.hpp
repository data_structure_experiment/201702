#ifndef INC_201702_SAVE_LOAD_HPP
#define INC_201702_SAVE_LOAD_HPP

#include <istream>
#include <ostream>
#include "double_linked_list.hpp"
#include "list_adapter.hpp"

template <typename T>
inline std::istream &operator>>(std::istream &in, double_linked_list<T> &list)
{
    list.clear();
    std::size_t size;
    in >> size;
    for (std::size_t i = 0; i < size; ++i)
    {
        T value;
        in >> value;
        list.insert(list.end(), std::move(value));
    }
    return in;
}
template <typename T>
inline std::ostream &operator<<(std::ostream &out, double_linked_list<T> const &list)
{
    out << list.size() << " ";
    for (auto const &each : list)
    {
        out << each << " ";
    }
    out << "\n";
    return out;
}
#endif //INC_201702_SAVE_LOAD_HPP
